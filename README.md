# Title

Some description

## Installation

To install the package, run the following command:


```shell

```

```shell
pip install 
```

## Usage

Here is an example of how to use the `DataCube` class:

```python

```

## Testing

First you need to install `pytest`, which is listed in the developper requirements

```shell
pip install pytest
```

Running the tests:
```shell
pytest
```


## Dependencies

This package depends on the following python packages:
* `numpy==1.24.1`
* `spectral==0.23.1`
* `matplotlib==3.6.3`