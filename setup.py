from setuptools import setup, find_packages

setup(
    name='sensor_rawdata_readers',
    version='1.0',
    packages=find_packages(),
    url='https://gitlab.switch.ch/ncwwm/datacube_processing',
    license='MIT',
    author='<Lechevallier Pierre>',
    author_email='<pierre.lechevallier@eawag.ch>',
    description='A package for processing hyperspectral datacubes',
    install_requires=['jcamp==1.2.2', 'pandas==1.5.3', 'scipy==1.10.0', 'matplotlib==3.6.3', 'seaborn==0.12.2']
)