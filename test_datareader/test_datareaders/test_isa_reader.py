from datareader.sensor_rawdata_readers.isa import _jcamp_reader, _extract_data_from_isa_jdx_file, \
    _extract_data_from_isa_txt_file, _reformat_isa_column_name

from datareader.sensor_rawdata_readers._lib import _validate_raw_data
import os
from datetime import datetime
import pandas as pd
import numpy as np


def test_extract_data_from_isa_jdx_files(test_data_folder_path):

    jdx_file_path = os.path.join(test_data_folder_path, 'spectra_isa_test.jdx')

    raw_data = _extract_data_from_isa_jdx_file(jdx_file_path)

    _validate_raw_data(raw_data)

    assert raw_data.shape == (3, 256)


def test_extract_data_from_isa_txt_file(test_data_folder_path):

    txt_file_path = os.path.join(test_data_folder_path, 'param_isa_test.txt')

    raw_data = _extract_data_from_isa_txt_file(txt_file_path)

    _validate_raw_data(raw_data)

    assert raw_data.shape == (9, 3)

def test_reformat_isa_column_name():
    # Test column name 'timestamp'
    col_name = 'timestamp'
    result = _reformat_isa_column_name(col_name)
    assert result == 'timestamp_[iso]'

    # Test column name with unit
    col_name = 'temperature [C]'
    result = _reformat_isa_column_name(col_name)
    assert result == 'temperature_[C]'

    # Test column name without unit
    col_name = 'pressure'
    result = _reformat_isa_column_name(col_name)
    assert result == 'pressure_[-]'