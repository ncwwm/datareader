from datareader.sensor_rawdata_readers.scan import _extract_data_from_scan_fp_files, _extract_data_from_scan_par_files, \
    _reformat_scan_column_name
from datareader.sensor_rawdata_readers._lib import _validate_raw_data
import os


def test_extract_data_from_scan_fp_files(test_data_folder_path):

    fp_file_path = os.path.join(test_data_folder_path, 'spectra_scan_test.fp')

    raw_data = _extract_data_from_scan_fp_files(fp_file_path)

    _validate_raw_data(raw_data)

    assert raw_data.shape == (8, 216)


def test_extract_data_from_scan_par_files(test_data_folder_path):

    par_file_path = os.path.join(test_data_folder_path, 'param_scan_test.par')

    raw_data = _extract_data_from_scan_par_files(par_file_path)

    _validate_raw_data(raw_data)

    assert raw_data.shape == (7, 4)

def test_reformat_scan_column_name():
    # Test column name 'timestamp'
    col_name = 'timestamp'
    result = _reformat_scan_column_name(col_name)
    assert result == 'timestamp_[iso]'

    # Test column name with unit
    col_name = 'temperature [C]'
    result = _reformat_scan_column_name(col_name)
    assert result == 'temperature_[C]'

    # Test column name without unit
    col_name = 'pressure'
    result = _reformat_scan_column_name(col_name)
    assert result == 'pressure_[-]'

