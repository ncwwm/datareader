from datareader.sensor_rawdata_readers.liquiline import _liquiline_reader_from_folder, _read_param_name_from_liquiline_filename
from datareader.sensor_rawdata_readers._lib import _validate_raw_data
import os

def test_liquiline_reader_from_folder(test_data_folder_path):

    fp_file_path = os.path.join(test_data_folder_path, 'param_liquiline_test/')

    raw_data = _liquiline_reader_from_folder(fp_file_path)

    _validate_raw_data(raw_data)

    assert raw_data.shape == (2, 8)

def test_read_param_name_from_filename():
    # Test case with a sample filename
    filename = 'some_useless_text param_name_2023_07_01.csv'
    expected_result = 'param_name'
    result = _read_param_name_from_liquiline_filename(filename)
    assert result == expected_result

    filename2 = 'some_useless_text PARAM NAME 2_2023_07_01.csv'
    expected_result2 = 'param_name_2'
    result2 = _read_param_name_from_liquiline_filename(filename2)
    assert result2 == expected_result2