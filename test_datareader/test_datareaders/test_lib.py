from datareader.sensor_rawdata_readers._lib import _check_and_convert_datetime_string, \
    _check_column_naming, _validate_raw_data, _removing_useless_data
import pytest
import pandas as pd
import numpy as np
from datetime import datetime


def test_check_column_naming():
    # Test valid column name
    col_name = "temperature_[C]"
    name, unit = _check_column_naming(col_name)
    assert name == "temperature"
    assert unit == "C"

    # Test column name missing opening bracket
    col_name = "temperature_C]"
    with pytest.raises(AssertionError):
        _check_column_naming(col_name)

    # Test column name missing closing bracket
    col_name = "temperature_[C"
    with pytest.raises(AssertionError):
        _check_column_naming(col_name)

    # Test column name with incorrect order of opening and closing brackets
    col_name = "temperature_]C["
    with pytest.raises(AssertionError):
        _check_column_naming(col_name)

    # Test invalid column name
    col_name = "pressure"
    with pytest.raises(AssertionError):
        _check_column_naming(col_name)

    # Test column name with multiple underscores
    col_name = "measurement_value_[mV]_filtered"
    name, unit = _check_column_naming(col_name)
    assert name == "measurement_value"
    assert unit == "mV"


def test_validate_raw_data_valid():
    valid_raw_data = pd.DataFrame({
        'timestamp_[iso]': pd.to_datetime(['2023-07-01T10:00:00', '2023-07-01T11:00:00', '2023-07-01T12:00:00']),
        'col1_[unit1]': [1, 2, 3],
        'col2_[unit2]': [4, 5, 6]
    })

    invalid_timestamps_raw_data = pd.DataFrame({
        'timestamp_[iso]': ['2023-07-01T10:00:00', '2023.11.07 11:00:00', '2023-07-01T12:00:00'],
        'col1_[unit1]': [1, 2, 3],
        'col2_[unit2]': [4, 5, 6]
    })

    invalid_numeric_column_raw_data = pd.DataFrame({
        'timestamp_[iso]': pd.to_datetime(['2023-07-01T10:00:00', '2023-07-01T11:00:00', '2023-07-01T12:00:00']),
        'col1_[unit1]': [1, 2, '3'],
        'col2_[unit2]': [4, 5, 6]
    })

    invalid_indexes_raw_data = pd.DataFrame({
        'timestamp_[iso]': pd.to_datetime(['2023-07-01T10:00:00', '2023-07-01T11:00:00', '2023-07-01T12:00:00']),
        'col1_[unit1]': [1, 2, 3],
        'col2_[unit2]': [4, 5, 6]
    }).set_index(np.arange(1, 4))

    # Test valid raw data
    _validate_raw_data(valid_raw_data)  # No assertion errors should be raised

    # Test raw data with invalid timestamps
    with pytest.raises(AssertionError):
        _validate_raw_data(invalid_timestamps_raw_data)

    # Test raw data with an invalid numeric column
    with pytest.raises(AssertionError):
        _validate_raw_data(invalid_numeric_column_raw_data)

    # Test raw data with invalid indexes
    with pytest.raises(AssertionError):
        _validate_raw_data(invalid_indexes_raw_data)


def test_removing_useless_data():
    sample_dataframe = pd.DataFrame({
        'col1': [1, 2, 3],
        'col2': [4, 4, 4],
        'col3': [np.nan, 5, 6],
        'col4': [7, 8, np.nan]
    })

    # Test curing the sample DataFrame
    expected_result = pd.DataFrame({
        'col1': [2],
        'col3': [5.0],
        'col4': [8.0]
    })
    cured_dataframe = _removing_useless_data(sample_dataframe)
    assert cured_dataframe.equals(expected_result)


def test_check_and_convert_datetime_string():
    # Test valid input
    input_str = '2022-05-01T12:30:00'
    expected_output = datetime.fromisoformat(input_str)
    assert _check_and_convert_datetime_string(input_str) == expected_output

    # Test invalid input
    input_str = 'invalid datetime'
    with pytest.raises(ValueError) as e:
        _check_and_convert_datetime_string(input_str)
        assert e == 'The string \'invalid datetime\' is not in iso format'
