import pytest
import os
import pandas as pd

@pytest.fixture
def test_data_folder_path():
    dir_path = os.path.dirname(__file__)
    return dir_path+'\\test_data\\'


