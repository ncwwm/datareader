from datareader.sensor_rawdata_readers.level import _extract_level_data
from datareader.sensor_rawdata_readers._lib import _validate_raw_data
import os

def test_extract_level_data(test_data_folder_path):

    level_file_path = os.path.join(test_data_folder_path, 'param_level_test.csv')

    raw_data = _extract_level_data(level_file_path)

    _validate_raw_data(raw_data)

    assert raw_data.shape == (9, 3)

    assert raw_data.loc[0, 'flow_[m3/h]'] == 0.09
    assert raw_data.loc[0, 'level_[cm]'] == 31.24422