import sqlite3
from datetime import datetime

import numpy as np
import pandas as pd


def _check_and_convert_datetime_string(a_string: str):
    try:
        a_string = datetime.fromisoformat(a_string)
    except:
        raise ValueError(f'The string \'{a_string}\' is not in iso format')
        # If the input string is not in ISO format, raise a ValueError exception with an informative message.
        # The exception will be caught by the caller of this function, allowing them to handle the error.


def _filter_data_by_time(data, start, end):
    if start:
        data = data[data['timestamp_[iso]'] >= start]
    if end:
        data = data[data['timestamp_[iso]'] <= end]
    return data


def _separate_data_unit(df: pd.DataFrame):
    unit_dict = {}

    for c in df.columns:
        n, u = c.split('_[')
        u = u[:-1]
        unit_dict[n] = u
        df.rename(columns={c: n}, inplace=True)

    return df, unit_dict


def _process_data(data, start, end):
    data['timestamp_[iso]'] = pd.to_datetime(data['timestamp_[iso]']).dt.round('2min')
    data = _filter_data_by_time(data, start, end)
    #         data['timestamp_[iso]'] = (data['timestamp_[iso]']).dt.isoformat()
    data, unit = _separate_data_unit(data)
    return data, unit


class SQLDataReader:

    def __init__(self, sql_db_path: str):
        if not sql_db_path.endswith('.sqlite'):
            raise ValueError('The path must be a .sqlite file')

        self._sql_db_path = sql_db_path
        self._connection = self._create_connection()
        self.table_list = self._extract_table_list()

    def get_data_pandas(self, table_name: str = 'data', start: str = None, end: str = None) -> tuple:

        if start:
            _check_and_convert_datetime_string(start)
        if end:
            _check_and_convert_datetime_string(end)

        if table_name not in self.table_list:
            return pd.DataFrame({}), {}

        data = pd.read_sql_query(f"SELECT * FROM {table_name}", self._connection, index_col='id')
        data, unit = _process_data(data, start, end)

        data.timestamp = pd.to_datetime(data.timestamp)
        data.drop_duplicates(subset='timestamp', inplace=True)

        data.index = np.arange(len(data.index))

        return data, unit

    def get_raw_data_pandas(self, table_name: str = 'data', start: str = None, end: str = None) -> pd.DataFrame:

        if start:
            _check_and_convert_datetime_string(start)
        if end:
            _check_and_convert_datetime_string(end)

        if table_name not in self.table_list:
            return pd.DataFrame({})

        data = pd.read_sql_query(f"SELECT * FROM {table_name}", self._connection, index_col='id')
        data = _filter_data_by_time(data, start, end)
        data.index = np.arange(len(data.index))

        return data

    def _create_connection(self):
        try:
            connection = sqlite3.connect(self._sql_db_path)
            print("Connection to SQLite DB successful")
            return connection
        except sqlite3.Error as e:
            raise ConnectionError(f"The error '{e}' occurred")

    def _execute_read_query(self, query):
        try:
            cursor = self._connection.cursor()
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except sqlite3.Error as e:
            print(f"The error '{e}' occurred")
            return []

    def _extract_table_list(self) -> list:
        read_query = """
            SELECT name FROM sqlite_master
            WHERE type='table'
            ORDER BY name;
            """
        table_list = [i[0] for i in self._execute_read_query(read_query)]
        return table_list
