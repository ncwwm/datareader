from sqlite3 import Error
import pandas as pd
import os
from datetime import datetime
from datareader.sensor_rawdata_readers._lib import _validate_raw_data

def detect_files_to_load(available_data: list, loaded_filed_df: pd.DataFrame):
    files_to_load = []

    for f in available_data:
        if f not in loaded_filed_df['file_path'].values:
            files_to_load.append(f)
        else:
            i = loaded_filed_df[loaded_filed_df['file_path'] == f].index
            past_size = loaded_filed_df.loc[i, 'file_size'].values[0]
            current_size = os.path.getsize(f)
            if past_size != current_size:
                files_to_load.append(f)
    return files_to_load

def _check_and_convert_datetime_string(a_string: str):
    """Converts a string to a datetime object if it is in ISO format.

    Args:
        a_string (str): A string to convert to a datetime object.

    Returns:
        datetime: A datetime object converted from the input string.

    Raises:
        ValueError: If the input string is not in ISO format.
    """
    try:
        a_string = datetime.fromisoformat(a_string)
    except:
        raise ValueError(f'The string \'{a_string}\' is not in iso format')
        # If the input string is not in ISO format, raise a ValueError exception with an informative message.
        # The exception will be caught by the caller of this function, allowing them to handle the error.

    return a_string

class SQLDataManager:

    def __init__(self, sql_db_path: str, data_folder_path: str, data_reader_func):

        assert sql_db_path[-6:] == 'sqlite', 'The path mush be a .sqlite file'

        self.data_reader = {'f': data_reader_func}

        self._sql_db_path = sql_db_path  # make private and rename sql_path
        self._connection = self._create_connection()

        self._data_folder_path = data_folder_path

        self.table_list = self._extract_table_list()

        if not self.table_list:
            self._load_data_for_the_first_time()

    def get_table_as_df(self, table_name: str = 'data') -> pd.DataFrame:
        if table_name in self.table_list:
            data = pd.read_sql_query(f"SELECT * FROM {table_name}", self._connection, index_col='id')
        else:
            data = pd.DataFrame({})
        return data

    def update_db(self, n_files_to_load=1000):

        available_data_source = [os.path.join(self._data_folder_path, f) for f in os.listdir(self._data_folder_path)]

        loaded_files = self.get_table_as_df('loaded_files')

        files_to_load = detect_files_to_load(
            available_data=available_data_source,
            loaded_filed_df=loaded_files
        )

        failed_files = []

        loaded_data = self.get_table_as_df('data')

        loaded_data['timestamp_[iso]'] = pd.to_datetime(loaded_data['timestamp_[iso]'])

        for f in files_to_load[:n_files_to_load]:

            try:
                raw_data = self._raw_data_reader(f)
            except:
                raw_data = pd.DataFrame({})
                failed_files.append(f)

            if not raw_data.empty:

                try:
                    _validate_raw_data(raw_data)

                    loaded_data = pd.concat([
                        loaded_data,
                        raw_data
                    ], ignore_index=True)

                    size = os.path.getsize(f)
                    loaded_files = pd.concat([
                        loaded_files,
                        pd.DataFrame({'file_path': [f], 'file_size': [size], })
                    ], ignore_index=True)

                except:
                    failed_files.append(f)



        loaded_data.drop_duplicates(subset='timestamp_[iso]', inplace=True)  ### suboptimal, to change
        loaded_data.index = np.arange(loaded_data.shape[0])

        _validate_raw_data(loaded_data)

        loaded_data.to_sql(name='data', con=self._connection, if_exists='replace', index_label='id')

        loaded_files.to_sql(name='loaded_files', con=self._connection, if_exists='replace', index_label='id')

        self.table_list = self._extract_table_list()

        print('The database has been updated. Failed files:')
        print(failed_files)

    def _create_connection(self):
        connection = None
        try:
            connection = sqlite3.connect(self._sql_db_path)
            print("Connection to SQLite DB successful")
        except Error as e:
            print(f"The error '{e}' occurred")

        return connection

    def _execute_query(self, query):
        cursor = self._connection.cursor()
        try:
            cursor.execute(query)
            self._connection.commit()
        except Error as e:
            print(f"The error '{e}' occurred")

    def _execute_read_query(self, query):
        cursor = self._connection.cursor()
        result = None
        try:
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Error as e:
            print(f"The error '{e}' occurred")

    def _extract_table_list(self) -> list:
        read_query = """
            SELECT name FROM sqlite_master
            WHERE type='table'
            ORDER BY name;
            """

        table_list = [i[0] for i in self._execute_read_query(read_query)]

        return table_list

    def _load_data_for_the_first_time(self):
        files_to_load = [os.path.join(self._data_folder_path, f) for f in os.listdir(self._data_folder_path)]

        data = pd.DataFrame({})

        loaded_files = pd.DataFrame({
            'file_path': [],
            'file_size': [],
        })

        failed_files = []

        # check data

        for f in files_to_load:

            # reading the data
            try:
                raw_data = self._raw_data_reader(f)
            except:
                raw_data = pd.DataFrame({})
                failed_files.append(f)

            if not raw_data.empty:

                if not data.empty:
                    data = pd.concat([
                        data,
                        raw_data
                    ], ignore_index=True)

                else:
                    data = raw_data

                size = os.path.getsize(f)
                loaded_files = pd.concat([
                    loaded_files,
                    pd.DataFrame({'file_path': [f], 'file_size': [size], })
                ], ignore_index=True)

        print('The data have been loaded for the first time. Failed files:')
        print(failed_files)


        data.drop_duplicates(subset='timestamp_[iso]', inplace=True)  ### suboptimal, to change
        data.index = np.arange(data.shape[0])

        # _validate_raw_data(data)

        data.to_sql(name='data', con=self._connection, if_exists='replace', index_label='id')

        loaded_files.to_sql(name='loaded_files', con=self._connection, if_exists='replace', index_label='id')

        self.table_list = self._extract_table_list()

    def _raw_data_reader(self, file_path: str):
        return self.data_reader['f'](file_path)


# datareader to read sqlite data

import sqlite3
import pandas as pd
import numpy as np


def _separate_data_unit(df: pd.DataFrame):
    unit_dict = {}

    for c in df.columns:
        n, u = c.split('_[')
        u = u[:-1]
        unit_dict[n] = u
        df.rename(columns={c: n}, inplace=True)

    return df, unit_dict