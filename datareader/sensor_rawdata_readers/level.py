import pandas as pd

def _extract_level_data(file_path) -> pd.DataFrame:
    """Extracts and transforms level data from a CSV file.

    Args:
        file_path (str): Path to the CSV file.

    Returns:
        pd.DataFrame: Processed level data.

    """
    # Read the CSV file into a DataFrame
    raw_data = pd.read_csv(file_path, sep=';')

    # Remove spaces from column names
    raw_data.rename(columns=lambda c: c.replace(' ', ''), inplace=True)

    # Strip whitespace from object-type columns
    raw_data = raw_data.apply(lambda x: x.str.strip() if x.dtype == "object" else x)

    # Combine 'DATE' and 'TIME' columns into 'timestamp' column
    raw_data['timestamp'] = pd.to_datetime(raw_data['DATE'] + ' ' + raw_data['TIME'], format='%d.%m.%Y %H:%M:%S')

    # Select and rename the desired columns
    raw_data = raw_data[['timestamp', 'trSSprgB091rValueOut', 'trSSprgB092rValueOut']].rename(columns={
        'timestamp': 'timestamp_[iso]', 'trSSprgB091rValueOut': 'level_[cm]', 'trSSprgB092rValueOut': 'flow_[m3/h]'
    })

    # Convert selected columns to float64 data type, ignoring errors
    raw_data['level_[cm]'] = raw_data['level_[cm]'].apply(pd.to_numeric, errors='coerce')
    raw_data['flow_[m3/h]'] = raw_data['flow_[m3/h]'].apply(pd.to_numeric, errors='coerce')

    return raw_data
