"""
Reading and combining liquiline files
"""

import pandas as pd
import os
from datareader.sensor_rawdata_readers._lib import _removing_useless_data


def _read_param_name_from_liquiline_filename(filename: str) -> str:
    """Reads the parameter name from the liquiline filename.

    Args:
        filename (str): The filename to extract the parameter name from.

    Returns:
        str: The extracted parameter name.
    """
    # Find the start index after the first space and before '_2023_'
    start_index = filename.find(' ') + 1
    end_index = filename.find('_2023_')

    # Extract the parameter name from the filename
    param_name = filename[start_index:end_index].replace(' ', '_').lower()

    return param_name


def _single_liquiline_file_reader(file_path: str):
    """reading a single liquiline fine
    
    Args:
        file_path (str): the path of the file to read

    Returns:
        data_raw (pd.DataFrame): the raw data 
        unit (str): the unit of the data

    """
    # reading the parameter name
    name = _read_param_name_from_liquiline_filename(file_path)

    #  reading the date and renaming the columns
    data_raw = pd.read_csv(
        file_path,
        delimiter=';',
        skiprows=3
    ).rename(columns={'Timestamp': 'timestamp', 'Measurement value': name})

    # extracting the data unit
    unit = data_raw.loc[0, 'Unit']

    # removing useless data
    data_raw = _removing_useless_data(data_raw)

    # keeping only the necessary columns
    try:
        data_raw = data_raw[data_raw['Sensor on hold'] == 'N'][['timestamp', name]]
    except:
        data_raw = data_raw[['timestamp', name]]

    # converting the timestamps from string to pd.timestamp
    data_raw['timestamp'] = pd.to_datetime(data_raw['timestamp'])

    return data_raw, unit


def _liquiline_reader_from_folder(folder_path: str) -> pd.DataFrame:
    """reading and combining data from different files contained in a folder

    Args:
        folder_path (str): the path to the folder containing liquiline data

    Returns:
        compiled_data (pd.DataFrame): the data combined in a dataframe

    """

    # Get the list of files in the input path
    file_arr = os.listdir(folder_path)

    # Initialize dictionaries to store data before merging
    data_dict = {}

    units = {'timestamp': 'iso'}

    # Loop over each file in the input path
    for file_name in file_arr:

        # check the file has the right format

        # Get the name and extension of the file
        filename, extension = os.path.splitext(file_name)

        # If the file extension is '.csv', try to read the data
        if extension == '.csv':
            temp_data, unit = _single_liquiline_file_reader(os.path.join(folder_path, file_name))
            name = temp_data.columns[1]
            i = 2
            while name in data_dict.keys():
                name = temp_data.columns[1] + str(i)
                i += 1
            temp_data.rename(columns={temp_data.columns[1]: name}, inplace=True)
            data_dict[name] = temp_data
            units[name] = unit

    # Combining the data
    for i, dat in enumerate(data_dict.values()):
        if i == 0:
            data_compiled = dat
        else:
            data_compiled = data_compiled.merge(right=dat)

    # renaming the columns including units:
    new_col_name = {
        col: f'{col}_[{units[col]}]' for col in data_compiled.columns
    }

    data_compiled.rename(columns = new_col_name, inplace=True)

    return data_compiled

