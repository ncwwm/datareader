import datetime
import os
import numpy as np
import pandas as pd

# TODO: tests

def _tss_data_reader(file_path: str) -> pd.DataFrame:
    """A function that reads and calculate tss from laboratory measurements

    Args:
        file_path (str): the data file path

    Returns:
        raw_data (pd.DataFrame): containing the raw data
    """

    # reading raw data
    raw_data = pd.read_csv(file_path, skiprows=[0, 1, 2])

    raw_data.dropna(inplace=True)

    raw_data['timestamp_[iso]'] = pd.to_datetime(raw_data['timestamp_[iso]'])

    # tss calculation
    raw_data['tss_[mg/l]'] = 1000 * (
                (raw_data['weight_filter_sample1_[mg]'] + raw_data['weight_filter_sample2_[mg]']) / 2 - (
                    raw_data['weight_filter1_[mg]'] + raw_data['weight_filter2_[mg]']) / 2) / raw_data['volum_[ml]']

    return raw_data[['timestamp_[iso]', 'tss_[mg/l]']]

def _turbidity_data_reader(file_path: str) -> pd.DataFrame:
    """
    Read turbidity data from a CSV file, clean and process it, and return a DataFrame with selected columns.

    Args:
        file_path (str): The path to the CSV file containing the turbidity data.

    Returns:
        pd.DataFrame: A DataFrame containing cleaned and processed turbidity data with selected columns.
    """

    raw_data = pd.read_csv(file_path, skiprows=[0, 1])

    raw_data.dropna(inplace=True)

    raw_data['timestamp_[iso]'] = pd.to_datetime(raw_data['timestamp_[iso]'])

    raw_data['turbidity_[ntu]'] = (raw_data['tur1_[ntu]'] + raw_data['tur2_[ntu]']) / 2

    return raw_data[['timestamp_[iso]', 'turbidity_[ntu]']]

def _lab_analytics_data_reader(file_path: str)-> pd.DataFrame:  ### TODO: simplify and test

    # Read lab values
    raw_lab_val = pd.read_excel(file_path, sheet_name='NonContact',
                                skiprows=[0, 1, 2, 3, 5, 6, 7])
    raw_lab_val = raw_lab_val[['Sample', 'Unnamed: 7', 'DOC', 'PO4-P', 'SO4', 'NH4-N', 'Nsol', 'TOC', 'Ntot']]
    raw_lab_val.columns = ['timestamp', 'time', 'doc', 'po4', 'so4', 'nh4', 'nsol', 'toc', 'ntot']
    raw_lab_val.dropna(subset=['timestamp', 'time'], inplace=True)

    # Convert column data types
    numeric_cols = ['doc', 'po4', 'so4', 'nh4', 'nsol', 'toc', 'ntot']
    raw_lab_val[numeric_cols] = raw_lab_val[numeric_cols].apply(pd.to_numeric, errors='coerce')
    raw_lab_val = raw_lab_val[raw_lab_val.time != ' ']
    raw_lab_val['timestamp'] = pd.to_datetime(raw_lab_val['timestamp'])

    # Update timestamp
    for i, row in raw_lab_val.iterrows():
        time = datetime.datetime.strptime(str(row['time']), '%H:%M:%S').time()
        delta = datetime.timedelta(hours=time.hour, minutes=time.minute)
        raw_lab_val.at[i, 'timestamp'] += delta

    # removing the time column
    raw_lab_val.drop(columns=['time'], inplace=True)

    raw_lab_val.rename(columns={
        'timestamp': 'timestamp_[iso]',
        'doc': 'doc_[mg/l]',
        'po4': 'po4_[mg/l]',
        'so4': 'so4_[mg/l]',
        'nh4': 'nh4_[mg/l]',
        'nsol': 'nsol_[mg/l]',
        'toc' : 'toc_[mg/l]',
        'ntot': 'ntot_[mg/l]'
    }, inplace=True)

    # renaming the index column
    raw_lab_val.index.name = 'index'

    # reindexing the data
    raw_lab_val.index = np.arange(len(raw_lab_val.index))

    return raw_lab_val

def _lab_data_reader(folder_path: str) -> pd.DataFrame:

    tss_data = _tss_data_reader(os.path.join(folder_path, 'tss_measurements.csv'))

    tur_data = _turbidity_data_reader(os.path.join(folder_path, 'tur_measurement.csv'))

    pol = _lab_analytics_data_reader(os.path.join(folder_path, 'LabResults2023_NonContact.xlsx'))

    merged_data = pol.merge(tss_data, how='outer').merge(tur_data, how='outer')

    merged_data.drop_duplicates(subset='timestamp_[iso]', inplace=True)

    merged_data.sort_values(by='timestamp_[iso]', inplace=True)

    merged_data.index = np.arange(len(merged_data.index))

    return merged_data