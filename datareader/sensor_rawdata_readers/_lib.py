"""
Library of different small functions for the module
"""

import pandas as pd
import numpy as np
from datetime import datetime

def _removing_useless_data(df: pd.DataFrame) -> pd.DataFrame:
        """Cures a pandas DataFrame by removing columns with constant values and rows with NaN values.

        Args:
            df (pd.DataFrame): The DataFrame to be cured.

        Returns:
            pd.DataFrame: The cured DataFrame.

        """
        # Remove columns with constant values
        df = df[[col for col in df.columns if df[col].nunique() > 1]]

        # Remove rows with NaN values
        df = df.dropna()

        # Reset the index to be sequential
        df.index = np.arange(df.shape[0])

        return df


def _validate_raw_data(raw_data: pd.DataFrame):
    """Validates the raw data DataFrame.

    Args:
        raw_data (pd.DataFrame): The raw data to validate.

    Raises:
        AssertionError: If the raw data fails any of the validation checks.
        ValueError: If the timestamps are not in isoformat.

    """
    _check_timestamp_column(raw_data)
    _check_columns(raw_data)
    _check_indexes(raw_data)


def _check_timestamp_column(raw_data: pd.DataFrame):
    assert 'timestamp_[iso]' in raw_data.columns, "The raw_data is missing the 'timestamp_[iso]' column."
    assert raw_data['timestamp_[iso]'].is_unique, "The 'timestamp_[iso]' column values are not unique."
    assert raw_data['timestamp_[iso]'].dtype == '<M8[ns]'


def _check_columns(raw_data: pd.DataFrame):
    for column in raw_data.columns:
        _check_column_naming(column)
        if column != 'timestamp_[iso]':
            assert pd.api.types.is_numeric_dtype(raw_data[column]), f"The '{column}' column is not numeric."


def _check_indexes(raw_data: pd.DataFrame):
    assert np.all(raw_data.index == np.arange(raw_data.shape[0])), "The DataFrame indexes are not sequential."



def _check_and_convert_datetime_string(a_string: str):
    """Converts a string to a datetime object if it is in ISO format.

    Args:
        a_string (str): A string to convert to a datetime object.

    Returns:
        datetime: A datetime object converted from the input string.

    Raises:
        ValueError: If the input string is not in ISO format.
    """
    try:
        a_string = datetime.fromisoformat(a_string)
    except:
        raise ValueError(f'The string \'{a_string}\' is not in iso format')
    return a_string


def _check_column_naming(col_name: str):
    """Checks if the column name is properly formatted and extracts the name and unit.

    Args:
        col_name (str): Column name to check.

    Returns:
        tuple: Tuple containing the name and unit extracted from the column name.

    Raises:
        ValueError: If the column name is not properly formatted (expected: name_[unit]).

    """

    assert '_[' in col_name, 'The column name is not properly formatted (expected: name_[unit])'
    assert ']' in col_name, 'The column name is not properly formatted (expected: name_[unit])'
    assert col_name.index('_[') < col_name.index(
        ']'), 'The column name is not properly formatted (expected: name_[unit])'

    try:
        name, unit = col_name.split('_[')
        unit = unit.split(']')[0]
    except ValueError:
        raise ValueError('The column name is not properly formatted (expected: name_[unit])')

    return name, unit