import pandas as pd
from datareader.sensor_rawdata_readers._lib import _removing_useless_data


def _extract_data_from_scan_fp_files(file_path: str) -> pd.DataFrame:
    """Extracts data from .fp files from s::can.

    Args:
        file_path (str): Path to the .fp file.

    Returns:
        pd.DataFrame: Extracted data from the .fp file.

    Raises:
        AssertionError: If the file_path doesn't have the '.fp' extension.

    """
    assert file_path[-3:] == '.fp', 'The scan param reader takes .fp files as argument'

    # Read the .fp file into a DataFrame, skipping the first row and renaming 'Date/Time' column
    data_raw = pd.read_csv(
        file_path,
        delimiter='\t',
        skiprows=1
    ).rename(columns={'Date/Time': 'timestamp'})

    # Remove useless columns
    data_raw = _removing_useless_data(data_raw)

    # Convert the timestamps to ISO format
    data_raw.timestamp = pd.to_datetime(data_raw.timestamp, format='%Y.%m.%d  %H:%M:%S')

    # Update the column names using _reformat_column_name
    data_raw.rename(columns={
        col: _reformat_scan_column_name(col) for col in data_raw.columns
    }, inplace=True)

    return data_raw


def _extract_data_from_scan_par_files(file_path: str) -> pd.DataFrame:
    """Extracts data from .par files generated from s::can.

    Args:
        file_path (str): Path to the .par file.

    Returns:
        pd.DataFrame: Extracted data from the .par file.

    Raises:
        AssertionError: If the file_path doesn't have the '.par' extension.

    """
    assert file_path[-4:] == '.par', 'The scan param reader takes .par files as argument'

    # Read the .par file into a DataFrame, skipping the first row and renaming 'Date/Time' column
    data_raw = pd.read_csv(
        file_path,
        delimiter='\t',
        encoding='unicode_escape',
        skiprows=1
    ).rename(columns={'Date/Time': 'timestamp'})

    # Remove useless columns
    data_raw = _removing_useless_data(data_raw)

    # Remove columns enclosed in brackets []
    for c in data_raw.columns:
        if f'{c[0]}{c[-1]}' == '[]':  # if the column name is between brackets
            data_raw = data_raw.drop(columns=[c])

    # Convert the timestamps to ISO format
    data_raw.timestamp = pd.to_datetime(data_raw.timestamp, format='%Y.%m.%d  %H:%M:%S')

    # Update the column names using _reformat_column_name
    data_raw.rename(columns={
        col: _reformat_scan_column_name(col) for col in data_raw.columns
    }, inplace=True)

    return data_raw


def _reformat_scan_column_name(col_name: str) -> str:
    """
    Reformating the column names to the proper format
    Args:
        col_name: the column name to reformat

    Returns:
        The updated column name
    """

    if col_name == 'timestamp':
        unit = 'iso'
        name = 'timestamp'

    else:
        try:
            # Find the index of the first '[' character
            start_unit_index = col_name.index('[') + 1

            # Find the index of the first ']' character after the '[' character
            end_unit_index = col_name.index(']', start_unit_index)

            # Extract the value between the '[' and ']' characters
            unit = col_name[start_unit_index:end_unit_index]
            unit = unit.replace(' ', '')

            # Extract the series of characters before the '[' character
            name = col_name[:start_unit_index - 1]

        except:
            unit = '-'
            name = col_name

        if unit == '':
            unit = '-'

    try:
        name = str(float(name))
    except:
        pass

    # Remove any spaces from the name
    name = name.replace(' ', '').replace('eq', '_eq').replace('-', '_').lower()

    # Return a tuple containing the extracted unit and name
    return f'{name}_[{unit}]'