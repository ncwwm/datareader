import jcamp
import pandas as pd
from datetime import datetime
from datareader.sensor_rawdata_readers._lib import _removing_useless_data

def _jcamp_reader(path: str):
    """
    A function to read a jcamp file
    Args:
        path(string):  the path file
    Returns:
        A pandas dataframe containing the spectra

    """
    # reading the file
    data_dict = jcamp.jcamp_readfile(path)

    # Extract column names from the JCAMP file
    col = data_dict['children'][0]['x']
    col = [str(i) for i in col]

    # Create a DataFrame to hold the data
    data_temp = pd.DataFrame(columns=col)

    # Convert columns to float, raising a ValueError if conversion fails
    # Having column name as float is required to plot the spectra
    for col in data_temp.columns:
        try:
            data_temp[col] = data_temp[col].astype(float)
        except ValueError as e:
            raise TypeError(f"Error converting column '{col}' to float: {e}")

    # Extract timestamps and data from the jcamp file
    timestamps_temp = pd.DataFrame(columns=['timestamp'])
    for i in range(len(data_dict['children'])):
        d = data_dict['children'][i]['date']
        t = data_dict['children'][i]['time']
        timestamps_temp.loc[i, 'timestamp'] = datetime.isoformat(
            datetime.strptime('{} {}'.format(d, t), '%y/%m/%d  %H:%M:%S'))

        data_temp.loc[i, data_temp.columns[0:]] = data_dict['children'][i]['y']

    data_temp = pd.concat([timestamps_temp, data_temp], axis=1)

    return data_temp


def _extract_data_from_isa_jdx_file(file_path: str):

    assert file_path[-4:] == '.jdx', 'The isa param reader takes .jdx files as argument'

    # Extract the data from the .txt files and concatenate them
    data_raw = _jcamp_reader(file_path)

    # Removing duplicates and useless data
    data_raw = _removing_useless_data(data_raw)

    # Conversion of the timestamps
    data_raw.timestamp = pd.to_datetime(data_raw.timestamp)

    # Updating the column names

    data_raw.rename(columns={
        col: _reformat_isa_column_name(col) for col in data_raw.columns
    }, inplace=True)

    return data_raw


def _extract_data_from_isa_txt_file(file_path: str):

    assert file_path[-4:] == '.txt', 'The isa param reader takes .txt files as argument'

    # Extract the data from the .txt files and renaming the timestamp column
    data_raw = pd.read_csv(file_path, delimiter='\t')

    data_raw = data_raw.rename(columns={data_raw.columns[0]: 'timestamp'})

    # Removing duplicates and useless data
    data_raw = _removing_useless_data(data_raw)

    # Converting timestamps
    data_raw.timestamp = pd.to_datetime(data_raw.timestamp, format='%d/%m/%Y %H:%M:%S')
    # data_raw.timestamp = data_raw.timestamp.map(lambda x: x.isoformat())

    # Updating the column names

    data_raw.rename(columns={
        col: _reformat_isa_column_name(col) for col in data_raw.columns
    }, inplace=True)

    return data_raw

def _reformat_isa_column_name(col_name: str):

    if col_name == 'timestamp':
        unit = 'iso'
        name = 'timestamp'

    else:
        try:
            # Find the index of the first '[' character
            start_unit_index = col_name.index('[') + 1

            # Find the index of the first ']' character after the '[' character
            end_unit_index = col_name.index(']', start_unit_index)

            # Extract the value between the '[' and ']' characters
            unit = col_name[start_unit_index:end_unit_index]
            unit = unit.replace(' ', '')

            # Extract the series of characters before the '[' character
            name = col_name[:start_unit_index - 1]

        except:
            unit = '-'
            name = col_name

        if unit == '':
            unit = '-'

    try:
        name = str(int(name[:-2]))
    except:
        pass

    # Remove any spaces from the name
    name = name.replace(' ', '').replace('-', '_').lower()

    # Return a tuple containing the extracted unit and name
    return f'{name}_[{unit}]'

