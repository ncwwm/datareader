import seaborn as sns
import pandas as pd


def plot_week_data(
        ax,
        week_nr: int,
        data: pd.DataFrame,
        ts_col: str,
        col: str,
        hue: str = None,
        plot_type: str = 'scatterplot',
        **kwargs
):
    ii = data[ts_col][data[ts_col].dt.isocalendar().week == week_nr].index

    if not data.loc[ii, :].empty:
        if plot_type == 'scatterplot':
            sns.scatterplot(ax=ax, data=data.loc[ii, :], x=ts_col, y=col, hue=hue, **kwargs)
        elif plot_type == 'lineplot':
            sns.lineplot(ax=ax, data=data.loc[ii, :], x=ts_col, y=col, hue=hue, **kwargs)


def plot_date_data(
        ax,
        date_str: str,
        data: pd.DataFrame,
        ts_col: str,
        col: str,
        hue: str = None,
        plot_type: str = 'scatterplot',
        **kwargs
):
    date_obj = pd.to_datetime(date_str)  # Convert date string to datetime object
    ii = data[ts_col][data[ts_col].dt.date == date_obj.date()].index

    if not data.loc[ii, :].empty:
        if plot_type == 'scatterplot':
            sns.scatterplot(ax=ax, data=data.loc[ii, :], x=ts_col, y=col, hue=hue, **kwargs)
        elif plot_type == 'lineplot':
            sns.lineplot(ax=ax, data=data.loc[ii, :], x=ts_col, y=col, hue=hue, **kwargs)

