import os
from datetime import datetime

import seaborn as sns
from IPython.core.display_functions import display
from matplotlib import pyplot as plt


def _unique_file_name(file_name: str, file_format: str='.png'):
    """
    Generating a unique name to save the pictures
    Args:
        file_name (string): file name
        file_format (string): default .png

    Returns: a unique file name
    """
    assert file_format in ['.png', '.jpeg'], 'This is not a valid picture format (.png or .jpeg)'

    if not os.path.exists(f'{file_name}.{file_format}'):
        unique_name = f'{file_name}.{file_format}'

    else:
        i = 1
        while os.path.exists(f"{file_name}_{i}.{file_format}"):
            i += 1

        unique_name = f'{file_name}_{i}.{file_format}'

    return unique_name


class SeabornTheme:
    """
    A class to set up the themes of the plots
    """
    def __init__(self, **kwargs):
        self.context = kwargs.get('context', 'notebook')
        self.style = kwargs.get('style', 'white')
        self.palette = kwargs.get('palette', 'tab10')
        self.font_scale = kwargs.get('font_scale', 1.5)
        self.rc = kwargs.get('rc',
                             {'axes.titlesize': 20.0, 'image.cmap': 'tab10', 'xtick.bottom': True, 'ytick.left': True,
                              'axes.grid': True, 'figure.edgecolor': 'black'})


class FigureKwargs:
    """
    A class to handle the kwargs of a matplotlib figure
    """
    def __init__(self,
                 default_figsize: tuple = (12, 8),
                 default_title: str = 'My figure',
                 default_layout: str = 'constrained',
                 default_sharex: str = False,
                 default_sharey: str = False,
                 **kwargs
                 ):
        self.figsize = kwargs.get('figsize', default_figsize)
        self.title = kwargs.get('title', default_title)
        self.layout = kwargs.get('layout', default_layout)
        self.sharex = kwargs.get('sharex', default_sharex)
        self.sharey = kwargs.get('sharey', default_sharey)


class SubPlots:
    """
    A class to generate a customized blank figure with axes
    """
    def __init__(self, nrows: int = 1, ncols: int = 1, **figure_kwargs):
        """
        Initializing the figure
        Args:
            nrows (int): number or rows (default: 1)
            ncols (int): number of columns (default: 1)
            **figure_kwargs: (see https://matplotlib.org/stable/api/figure_api.html)
                figsize (tuple) = (12, 8)
                sharex (bool) = False
                sharey (bool) = False
                layout (str) = 'constrained'
                title (str) = 'My figure'
        """
        # Setting the theme of the figure
        sns.set_theme(**SeabornTheme().__dict__)

        # Getting the keyword arguments
        kws = FigureKwargs(**figure_kwargs)

        # Generating a figure
        self.fig, self.ax = plt.subplots(nrows=nrows, ncols=ncols, figsize=kws.figsize, sharex=kws.sharex,
                                         sharey=kws.sharey, layout=kws.layout)
        self.fig.suptitle(kws.title)

    def show(self, save_fig: bool = False, save_path: str = None, file_format: str = 'png'):
        """
        Displaying and eventually saving the figure
        Args:
            save_fig (boolean): save the figure if True
            save_path (string): path to save the figure
            file_format (string): .png or .jpeg
        """
        display(self.fig)
        if save_fig:
            self.save(save_path, file_format)

    def save(self, save_path: str = None, file_format: str = 'png', overwrite: bool = False):
        """
        Saving the figure
        Args:
            save_path (string): the path of the figure to save (including figure name)
            file_format (string): .png or .jpeg
            overwrite (bool): default False. Else overwrite previous files
        """
        # generating a file name if necessary
        if save_path is None:
            save_path = datetime.now().strftime('%Y_%m_%d_%H_h_%M_min') + '_saved_figure'

        if not overwrite:
            save_path = _unique_file_name(save_path, file_format)

        self.fig.savefig(save_path)
