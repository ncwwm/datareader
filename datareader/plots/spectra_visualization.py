import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt

from datareader.plots._lib import SubPlots, FigureKwargs


class SpectraPlot(SubPlots):
    """
    Plotting the spectra
    """
    def __init__(self, data, **kwargs):
        """
        Generating the plot
        Args:
            data (pandas dataframe): a dataframe containing the spectra. The columns must be convertible to floats.
            **kwargs: (see https://matplotlib.org/stable/api/figure_api.html, https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.html#matplotlib.axes.Axes)
                figsize (tuple) = (12, 6)
                sharex (bool) = False (useless because single subplot)
                sharey (bool) = False (useless because single subplot)
                layout (str) = 'constrained'
                title (str) = 'Plot of the spectra'
                color (str) = first color of the current color palette
                alpha (str) = 0.2
                linestlyle (str) = '-'
                xlabel (str) = 'wavelength [nm]'
                ylabel (str) = 'absorbance [-]'
        """

        # Extracting spectra

        sp_df = data[[c for c in data.columns if c != 'timestamp']]

        # Figure kwargs
        fig_kwargs = FigureKwargs(default_figsize=(12, 6), default_title='Plot of the spectra', **kwargs).__dict__

        # Other kwargs
        color = kwargs.get('color', sns.color_palette(sns.axes_style()['image.cmap'])[0])
        alpha = kwargs.get('alpha', 0.2)
        linestyle = kwargs.get('linestyle', '-')
        xlabel = kwargs.get('xlabel', 'wavelength [nm]')
        ylabel = kwargs.get('ylabel', 'absorbance [-]')

        # Initialization of the figure
        super().__init__(nrows=1, ncols=1, **fig_kwargs)

        # Extraction of the wavelength from the data columns names
        wavelengths = np.array([float(w) for w in sp_df.columns])

        # Plotting of each spectrum
        for ind in sp_df.index:
            sns.lineplot(x=wavelengths, y=sp_df.loc[ind, :].values, ax=self.ax,
                         color=color, alpha=alpha, ls=linestyle, legend=False)

        # setting the figure axes labels
        self.ax.set(xlabel=xlabel, ylabel=ylabel)
        plt.close()
