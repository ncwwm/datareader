"""
Library of the different plotting functions for the module
"""

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import matplotlib.dates as mdates

from datareader.plots._lib import FigureKwargs, SubPlots


class ParamSubplot(SubPlots):
    def __init__(
            self,
            data: pd.DataFrame,
            unit: pd.DataFrame,
            timestamp: pd.DataFrame,
            col: list = [],
            **kwargs):
        """
        Plot of the measured parameters in a dataframe
        Args:
            data (pandas dataframe): the data
            unit (pandas dataframe):
            timestamp (pandas dataframe):
            col (list): a list of the column to plot. If None, all the data columns will be plotted.
            **kwargs: (see https://matplotlib.org/stable/api/figure_api.html, https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.html#matplotlib.axes.Axes)
                figsize (tuple) = (12, 4*ncols)
                sharex (bool) = True
                sharey (bool) = False
                layout (str) = 'constrained'
                title (str) = 'Plot of the measurements'
                marker (str) = 'o'
        """

        # preparation of the figure
        if not col:
            col = [c for c in data.columns if c!='timestamp']

        else:
            for c in col:
                assert c in data.columns, 'This column name is not valid'

        n_subplots = len(col)
        col = sorted(col) # alphabetical ordering

        fig_kws = FigureKwargs(default_figsize=(12, 4 * n_subplots),
                               default_sharex=True,
                               default_title='Plot of the measurements',
                               **kwargs).__dict__

        marker = kwargs.get('marker', 'o')

        default_ylabel = [f'{c} [{unit[c].values[0]}]' for c in col]
        x = timestamp['timestamp']

        # Initialization of the figure
        super().__init__(nrows=n_subplots, ncols=1, **fig_kws)

        # Drawing of each axe
        for i, c in enumerate(col):

            y = data[c]

            if len(col) > 1:
                sns.scatterplot(ax=self.ax[i], x=x, y=y, marker=marker)
                self.ax[i].set_ylabel(default_ylabel[i])

            if len(col) == 1:
                sns.scatterplot(ax=self.ax, x=x, y=y, marker=marker)
                self.ax.set_ylabel(default_ylabel[i])

        # Changing the timestamps format of the the x axis
        dtFmt = mdates.DateFormatter('%m-%d %H:%M')  # define the formatting
        plt.gca().xaxis.set_major_formatter(dtFmt)  # apply the format to the desired axis

        # rotation of the xticks
        plt.xticks(rotation='vertical')

        plt.close()

class ParamSubplot2(SubPlots):
    def __init__(
            self,
            data: pd.DataFrame,
            unit: dict = {},
            timestamp_col: str = 'timestamp',
            col_to_plot: list = [],
            label_col: str = None,
            **kwargs):

        # preparation of the figure
        if not col_to_plot:
            col_to_plot = [c for c in data.columns if c != timestamp_col]
        else:
            for c in col_to_plot:
                assert c in data.columns, 'This column name is not valid'

        if label_col:
            assert label_col in data.columns
            assert data[label_col].dtype == 'object'
            assert len(set(data[label_col].values))<5

            lab_list = list(set((data[label_col].values)))

        n_subplots = len(col_to_plot)
        col_to_plot = sorted(col_to_plot) # alphabetical ordering

        fig_kws = FigureKwargs(default_figsize=(12, 4 * n_subplots),
                               default_sharex=True,
                               default_title='Plot of the measurements',
                               **kwargs).__dict__

        marker = kwargs.get('marker', 'o')

        if unit:
            default_ylabel = [f'{c} [{unit[c].values[0]}]' for c in col_to_plot]
        else:
            default_ylabel = col_to_plot

        x = data[timestamp_col]

        # Initialization of the figure
        super().__init__(nrows=n_subplots, ncols=1, **fig_kws)

        # Drawing of each axe
        for i, c in enumerate(col_to_plot):

            if len(col_to_plot) > 1:

                if label_col:
                    for lab in lab_list:
                        y = data.loc[data[label_col] == lab, c]
                        sns.scatterplot(ax=self.ax[i], x=x, y=y, marker=marker, label =lab)
                    self.ax[i].legend()
                    self.ax[i].set_ylabel(default_ylabel[i])
                else:
                    y = data[c]
                    sns.scatterplot(ax=self.ax[i], x=x, y=y, marker=marker)
                    self.ax[i].set_ylabel(default_ylabel[i])

            if len(col_to_plot) == 1:

                if label_col:
                    for lab in lab_list:
                        y = data.loc[data[label_col] == lab, c]
                        sns.scatterplot(ax=self.ax, x=x, y=y, marker=marker, label =lab)
                    self.ax.legend()
                    self.ax.set_ylabel(default_ylabel[i])
                else:
                    y = data[c]
                    sns.scatterplot(ax=self.ax, x=x, y=y, marker=marker)
                    self.ax.set_ylabel(default_ylabel[i])

        # Changing the timestamps format of the the x axis
        dtFmt = mdates.DateFormatter('%m-%d %H:%M')  # define the formatting
        plt.gca().xaxis.set_major_formatter(dtFmt)  # apply the format to the desired axis

        # rotation of the xticks
        plt.xticks(rotation='vertical')

        plt.close()

