import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from datareader.plots._lib import FigureKwargs, SubPlots

class WeekPlot(SubPlots):

    def __init__(self, df: pd.DataFrame, col_to_plot: str, timestamp_col: str = 'timestamp',
                 week_per_plot: int = 1, ylim: list = [], label_col:str=None, **kwargs):

        assert timestamp_col in df.columns, 'Please provide the name of the column containing timestamps'
        assert col_to_plot in df.columns, f'The column \'{col_to_plot}\' is not in the data'
        assert df[timestamp_col].dtype == '<M8[ns]', f'The column \'{timestamp_col}\' should contain pd.timestamps'
        if label_col:
            assert label_col in df.columns
            assert df[label_col].dtype == 'object'
            assert len(set(df[label_col].values))<5

        df = df.copy()
        w_min = np.datetime64(min(df[timestamp_col].values), 'W')

        # adding a column with week number
        df['week_nr'] = [(np.datetime64(df.loc[ind, timestamp_col], 'W') - w_min).astype('int') for ind in df.index]

        total_week_nr = _get_total_week_nr(df[timestamp_col].values)

        nb_of_subplots = total_week_nr // week_per_plot + total_week_nr % week_per_plot

        # Figure kwargs
        fig_kwargs = FigureKwargs(default_figsize=(20, 3 * nb_of_subplots), **kwargs).__dict__

        # Other kwargs
        color = kwargs.get('color', sns.color_palette(sns.axes_style()['image.cmap'])[0])
        alpha = kwargs.get('alpha', 0.2)
        linestyle = kwargs.get('linestyle', '-')
        xlabel = kwargs.get('xlabel', 'wavelength [nm]')
        ylabel = kwargs.get('ylabel', 'absorbance [-]')

        # Initialization of the figure
        super().__init__(nrows=nb_of_subplots, ncols=1, **fig_kwargs)

        for w in range(total_week_nr):

            if label_col:

                for i, val in enumerate(set(df[label_col].values)):

                    temp_data = df.loc[[df.loc[ii, 'week_nr'] == w and df.loc[ii, label_col]==val for ii in df.index], :]

                    sns.scatterplot(data=temp_data, x=timestamp_col, y=col_to_plot,
                            ax=self.ax[w // week_per_plot], color=sns.color_palette(sns.axes_style()['image.cmap'])[i])

            else:
                sns.scatterplot(data=df[df['week_nr'] == w], x=timestamp_col, y=col_to_plot,
                            ax=self.ax[w // week_per_plot], color=color)

            if w % week_per_plot == 0:

                x_min = np.datetime64(min((df[df['week_nr'] == w])[timestamp_col].values), 'W')

                self.ax[w // week_per_plot].set_xlim([x_min, x_min + np.timedelta64(7 * week_per_plot, 'D')])

                if ylim:
                    self.ax[w // week_per_plot].set_ylim(ylim)


        plt.close()

def _get_total_week_nr(ts_list):
    w_min = np.datetime64(min(ts_list), 'W')
    w_max = np.datetime64(max(ts_list), 'W')
    delta = w_max - w_min
    return delta.astype('int') + 1

